			+--------------------+
			|        CS 521      |
			| PROJECT 1: THREADS |
			|   DESIGN DOCUMENT  |
			+--------------------+
				   
---- GROUP ----
>> Fill in the names and email addresses of your group members.
############  Tarun Gauba <tarungau@buffalo.edu>  ################

---- PRELIMINARIES ----
>> If you have any preliminary comments on your submission, notes for the
>> TAs, or extra credit, please give them here.

>> Please cite any offline or online sources you consulted while
>> preparing your submission, other than the Pintos documentation, course
>> text, lecture notes, and course staff.

			     ALARM CLOCK
			     ===========

---- DATA STRUCTURES ----

>> A1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

Data Structures and Functions Used -
1. static struct list blocked_thread_list;
A list to hold all the blocked threads. This is mantained sorted(by sleep_time) as we want them to wake them in order by sleep_time.

2. static struct lock lock_ordered_list;
A lock which thread uses before entering C.S(Modifying blocked_thread_list).

3. In struct thread {
    int64_t sleep_time;			/* Time for which current thread is sleeping */
    struct semaphore self_block_sema; 	/* Semaphore for that so that its sleeping and waking up activity can be synchronized. Thread is blocked by decrementing this only. Inittialized to 0 */
    struct list_elem sleeping_elem;	/* List element for Threads which were sleeping on Timer Busy Wait Condition */
}

---- ALGORITHMS ----

>> A2: Briefly describe what happens in a call to timer_sleep(),
>> including the effects of the timer interrupt handler.
  
  1. In timer_sleep() firstly I am making sure that external interrupt is not disabled for synchronization. For this I am using Lock and Semaphore to sync activity of threads. Internally Semaphore (and Lock) also blocks Hardware interrupt, but as we were specifically told to implement this using sync variables, so I am using these over critical section.
  2. After acquiring proper locks and updating sleep_time of thread (sleep_time is an int of thread - individual property of every thread), then I am inserting thread in blocked_thread_list(C.S - Shared data structure) in a ordered way (Insertion Sort method exact), so a comparator function is also used for this. Then after lock is released, thread downs it's individual semaphore, just to block itself from running.

>> A3: What steps are taken to minimize the amount of time spent in
>> the timer interrupt handler?
  1. To minimize amount of time spent in timer_interrupt_handler, in timer_sleep() I am inserting threads in blocked list in sorted order. This way timer interrupt handler has just to take first element from the head of list and unblock it. Otherwise, it would have to scan whole list to find thread with minimum sleep_time and that is O(n) complexity.
  2. Basically in this function we have to wake up all the threads with same wakeup time in same function call, so we iterate till sleep_time isn't greater than current_ticks time. Then whichever thread needs to be awake is unblocked(by incrementing it's individual semaphore) and removed from blocked_thread_list. I drew this case that when we want processes to be executed in some specific order, the one we want to happen after is blocked on semaphore initialized to 0 and other process when signals(increments) semaphore can only let first one to work.

*/
---- SYNCHRONIZATION ----

>> A4: How are race conditions avoided when multiple threads call
>> timer_sleep() simultaneously?
   - Using locks we are avoiding race conditions(in CS - blocked_list) when multiple calls to timer_sleep() occurs.
>> A5: How are race conditions avoided when a timer interrupt occurs
>> during a call to timer_sleep()?
  - Locks (Semaphore initialized to 1 with sngle parent thread) are used to manage critical section (blocked thread list) in timer_sleep. So even if a timer interrupt occurs when timer_sleep is in critical section, timer_sleep will do its critic l tasks first then only can be pre-empted due to any external interrupt.

---- RATIONALE ----

>> A6: Why did you choose this design?  In what ways is it superior to
>> another design you considered?
Few other designs with flaws I considered for this case -
1. Disable the interrupt, put the thread in ordered_list ad then re-enabling it. But this was strictly 'NO' from the Professor just because that will defeat our overall learning purpose of the task.
2. Make semaphore block list as ordered and then insert into that list. Flaw - Making semaphore blocked list ordered by sleep_time will hamper further tasks of priority-sema. So this isn't a good idea.
3. Using a semaphor in both producer and consumer (timer_sleep and timer_interrupt) for sync over critical section. But this doesn't work well due to some programming logical errors. I wish to try, learn and implement this in the future. **

			 PRIORITY SCHEDULING
			 ===================

---- DATA STRUCTURES ----

>> B1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.


>> B2: Explain the data structure used to track priority donation.
>> Use ASCII art to diagram a nested donation.  (Alternately, submit a
>> .png file.)
Did not implemented priority donation as preious test cases were failing when I tried implementing this. I will try improving on it for sure.

---- ALGORITHMS ----

>> B3: How do you ensure that the highest priority thread waiting for
>> a lock, semaphore, or condition variable wakes up first?

>> B4: Describe the sequence of events when a call to lock_acquire()
>> causes a priority donation.  How is nested donation handled?

>> B5: Describe the sequence of events when lock_release() is called
>> on a lock that a higher-priority thread is waiting for.

---- SYNCHRONIZATION ----

>> B6: Describe a potential race in thread_set_priority() and explain
>> how your implementation avoids it.  Can you use a lock to avoid
>> this race?

---- RATIONALE ----

>> B7: Why did you choose this design?  In what ways is it superior to
>> another design you considered?

I was not able to implement priority donation tasks as prior tests were failing when I tried doing so. I just completed simple priority_scheduler/ priority-sema and priority-condvar with all it's test cases.

My Overall design and Reasons for same -
-----------------
Simple Priority Scheduling Implementation -

1. Changes in thread_unblock, thread_yeild, thread_create, thread_set_priority. I made changes in all these functions are these are the entry points to ready queue (we can see the process state diagram) or only at these points priority changes takes place.

2. In thread_create checking if created thread priority is greater than main thread priority. If it is, then yield is called on current thread and it pre-empts as higher priority thread is now there in the ready queue.

3. In thread_unblock() - When thread is unblocked and it is being put in ready queue, insert that in ordered ready list. Earlier it was put from end of ready queue. This way when next_thread_to_run is called, it will pick head element from ready_queue which is automatically highest priority thread which is ready. Hence priority ordering is implemented. For mantaining order, we use thread_priority_comparator function for insertion into ready list we insert in insertion sort.

4. In thread_yield() - When thread is yielded and it is being put in ready queue, insert that in ordered ready list. This way when next_thread_to_run is called, it will pick head element from ready_queue which is automatically highest priority thread which is ready. Hence priority ordering is implemented. For mantaining order, we use thread_priority_comparator function for insertion into ready list we insert in insertion sort.

5. In thread_set_priority(int priority) - First we set the new priority, then we check the Running thread's priority if less than maximum priority in Ready Queue, if yes then Currently running thread is pre-empted and automatically, higher priority thread will start to run.

6. Thread_get_priority - returns the current_thread() priority.


This is the most simple and optimized design possible for cases I considered and this works well so I considered this design.


7. Implementation of Priority-sema 
In sema_down() I am inserting into ordered list of semaphore, so this way priority is imlemented when sema_up wakes up threads from head of sema_list. When I wake up thread in sema_up, again check priority of Running thread with frontier thread of list. If running thread is having less priority that thread at head of list, then it is preempted. This is applied to locks automatically and locks semphores initialized to 1.

8. Implementation of Priority Condition_Variable -
 Here semaphore's priority value is tested and compared and waiting list is sorted on basis of this priority. Rest it is similar to priority-sema.
 

			  ADVANCED SCHEDULER
			  ==================

---- DATA STRUCTURES ----

>> C1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.
- In thread.h -
Added following elements in struct thread -
struct thread {
    int nice_val;			/* Nice Value of Current Thread. Used in MLFQS */
    int recent_cpu;			/* Recent CPU value of Current Thread. Used in MLFQS */
}

/* Used for Floating Point Calculations */
#define F (1<<14) // This is equal to 2^14 in 17.14 system.

- In thread.c
/* Global Average Load for all threads. This is used to store most recent load_avg for the system in MLFQS. */
int load_avg;



---- ALGORITHMS ----
About the algorithm..
**I understood and implemented the BSD scheduler as directly mentioned in PintOS document step by step. 

>> C2: Suppose threads A, B, and C have nice values 0, 1, and 2.  Each
>> has a recent_cpu value of 0.  Fill in the table below showing the
>> scheduling decision and the priority and recent_cpu values for each
>> thread after each given number of timer ticks:

timer  recent_cpu    priority   thread
ticks   A   B   C   A   B   C   to run
-----  --  --  --  --  --  --   ------
 0
 4
 8
12
16
20
24
28
32
36

>> C3: Did any ambiguities in the scheduler specification make values
>> in the table uncertain?  If so, what rule did you use to resolve
>> them?  Does this match the behavior of your scheduler?

>> C4: How is the way you divided the cost of scheduling between code
>> inside and outside interrupt context likely to affect performance?
For scheduler, an external interrupt runs after fixed interval  and in this duration schedular can do administrative task of deciding new jobs, preemepting and scheduling jobs. But this quantum is also limited so schedular has to make quick decision else it's a design flaw and we will miss some time. So when setting tasks in timer_interrupt we should keep less time consuming things which occur in time on a whole.


---- RATIONALE ----

>> C5: Briefly critique your design, pointing out advantages and
>> disadvantages in your design choices.  If you were to have extra
>> time to work on this part of the project, how might you choose to
>> refine or improve your design?

Overall system design with advantages and flaws -
1. I mainly implemented 4 modules as mentioned in PintOS manual - 
   1.1 Nice - This is a property of thread. Initially I am setting it to 0. Otherwise threads inherit this from parent.
     1.1.1 thread_get_nice() - returns the nice value for current thread.
     1.1.2 thread_set_nice() - this sets a new nice value for thread. As nice value is reset, so I am recomputing thread's priority. If current thread priority is less than any of thread's in ready queue, I preempt it.
   1.2 Compute_priority() - For every thread this is computed at init. Then incremented by 1 for every running. Recomputed once per second for all threads in thread all list.
   1.3 Recent_cpu - Function to set/get this is compute_recent_cpu.
     1.3.1 Recomputed once per second for all threads. 
     1.3.2 Function is there is get_recent_cpu
   1.4 load_avg - Initialized to 0 at system boot.
     1.4.1 Computed once per second.
     1.4.2 Function is there is get_load_cpu

** If I would have got some extra time on this project - After understanding this well, I would have tested this system statistics for different configurations/ designs, implmented random designs and chose to implement most optimal from all for specific cases.

Flaws according to me of this overall system-
1. This all involves a lot of calculations, and work done by scheduler is waste of CPU cycles in the end(as it does management stuff only, no user level output of jobs). So these kind of decisions should be lightweight. Even heavy operations should be done in long term decision making.
2 In this system, all things are fixed, so according to me many creative solutions can be there to an OS according to the hardware and applications it is working with. So BSD4.4 implementation shouldn't be fixed as such with a fixed range of nice values etc. It should be tuned as per use. Example - For a single user running batch jobs not much of short term scheduling decisions are required so they can be skipped by fine tuning the algorithm.


>> C6: The assignment explains arithmetic for fixed-point math in
>> detail, but it leaves it open to you to implement it.  Why did you
>> decide to implement it the way you did?  If you created an
>> abstraction layer for fixed-point math, that is, an abstract data
>> type and/or a set of functions or macros to manipulate fixed-point
>> numbers, why did you do so?  If not, why not?

I implemented the arithematic for floating-point math directly in the functions it was required. I didn't for separate modules for the same. I realize that this is a bad programming practice and alot of hardcoding is required and reusabiity is also not taken care of. But in out case we required just 3-4 equations so before implementing full module set I did case specific implementations directly in place. As less requirements are there so reusability is also not much affected. Once this software grows, it would require separate modules/macros for these cases. We can also overload separate symbol set for these manipulations (# for multiply etc) 

Example - fptoi(int fp) 
          {
            return fp/F;
          }


			   SURVEY QUESTIONS
			   ================

Answering these questions is optional, but it will help us improve the
course in future quarters.  Feel free to tell us anything you
want--these questions are just to spur your thoughts.  You may also
choose to respond anonymously in the course evaluations at the end of
the quarter.

>> In your opinion, was this assignment, or any one of the three problems
>> in it, too easy or too hard?  Did it take too long or too little time?

>> Did you find that working on a particular part of the assignment gave
>> you greater insight into some aspect of OS design?

>> Is there some particular fact or hint we should give students in
>> future quarters to help them solve the problems?  Conversely, did you
>> find any of our guidance to be misleading?

>> Do you have any suggestions for the TAs to more effectively assist
>> students, either for future quarters or the remaining projects?

>> Any other comments?
